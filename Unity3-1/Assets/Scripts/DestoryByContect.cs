﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestoryByContect : MonoBehaviour {

    public GameObject explosion;
    public GameObject playerExplosion;
    public int scoreValue;

    private GameController gameController;

	// Use this for initialization
	void Start () {
        GameObject gameControllerObject = GameObject.FindWithTag("GameController");

        if (gameControllerObject != null)
        {
            gameController = gameControllerObject.GetComponent<GameController>();
        }
        if (gameControllerObject == null)
        {
            Debug.Log("can not find 'GameController' ");
        }
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Boundary")
        {
            return;
        }

        if (other.tag == "Player")
        {
            Instantiate(playerExplosion, other.transform.position, other.transform.rotation);       //폭발 효과 재생
            gameController.GameOver();
        }

        Instantiate(explosion, transform.position, transform.rotation);     //폭발 효과 재생
        Destroy(other.gameObject);      //bolt 파괴
        Destroy(gameObject);            //소행성 파괴

        gameController.AddScore(scoreValue);
    }
}
